package com.firemix.hmsremotconfig.Configs;

import android.content.Context;

import com.firemix.hmsremotconfig.R;
import com.github.javiersantos.bottomdialogs.BottomDialog;

public class MessageApp {
    private static MessageApp messageApp;
    private final MessageActions actions;
    private final Context ctx;

    private MessageApp(Context ctx, MessageActions actions) {
        this.ctx = ctx;
        this.actions = actions;
    }

    public static MessageApp getInstance(Context ctx, MessageActions actions) {
        if (ctx != null) {
            messageApp = new MessageApp(ctx, actions);
        }
        return messageApp;
    }

    public void LoadMessage(String Title, String Msj, boolean state) {
        new BottomDialog.Builder(ctx)
                .setTitle(Title)
                .setContent(Msj)
                .setPositiveText("SI")
                .setNegativeText("NO")
                .setCancelable(false)
                .setPositiveBackgroundColorResource(R.color.colorPrimary)
                .setPositiveTextColorResource(android.R.color.white)
                .onPositive(dialog -> {
                    if (state) {
                        actions.Accept(1);
                    } else {
                        actions.Accept(0);
                    }
                    dialog.dismiss();
                })
                .setNegativeTextColorResource(R.color.black)
                .onNegative(dialog -> actions.Cancel())
                .show();
    }


    public interface MessageActions {
        void Accept(int op);

        void Cancel();
    }
}

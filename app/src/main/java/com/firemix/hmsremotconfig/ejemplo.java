package com.firemix.hmsremotconfig;

import android.content.pm.PackageInfo;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.firemix.hmsremotconfig.Configs.HMSRemoteConfig;
import com.firemix.hmsremotconfig.Configs.MessageApp;
import com.firemix.hmsremotconfig.Configs.RemoteData;
import com.firemix.hmsremotconfig.Configs.UtilsApp;

public class ejemplo extends AppCompatActivity implements MessageApp.MessageActions, HMSRemoteConfig.HMSActions {
    private HMSRemoteConfig remoteConfig;
    private UtilsApp utilsApp;
    private MessageApp messageApp;
    private long actual;

    @RequiresApi(api = Build.VERSION_CODES.P)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        messageApp = MessageApp.getInstance(this,this);
        utilsApp = UtilsApp.getInstance();
        remoteConfig = HMSRemoteConfig.getInstance(this);
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            actual = packageInfo.getLongVersionCode();
            remoteConfig.Comparar();
        } catch (Exception e){

        }
    }

    @Override
    public void Accept(int op) {
        if(op == 1){
            finish();
        }
    }

    @Override
    public void Cancel() {

    }

    @Override
    public void CheckVersion() {
        RemoteData remoteData = utilsApp.obtenerDatos();
        if(remoteData.Version > actual){
            messageApp.LoadMessage(remoteData.Titulo,remoteData.Mensaje,remoteData.Estado);
        }
    }
}
